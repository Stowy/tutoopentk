﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using OpenToolkit.Graphics.OpenGL4;
using OpenToolkit.Mathematics;

namespace TutoOpenTK
{
    public class Shader : IDisposable
    {
        private readonly int handle;
        private bool disposedValue;
        private readonly Dictionary<string, int> uniformLocations;

        public Shader(string vertexPath, string fragmentPath)
        {
            int vertexShader;
            int fragmentShader;

            // Read the shaders source
            string vertexShaderSource = ReadShaderSource(vertexPath);
            string fragmentShaderSource = ReadShaderSource(fragmentPath);

            // Create the shaders and bind the source code to them
            vertexShader = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(vertexShader, vertexShaderSource);

            fragmentShader = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(fragmentShader, fragmentShaderSource);

            // Compile the shaders and print any errors
            CompileShader(vertexShader);
            CompileShader(fragmentShader);

            // We now make a program (handle) out of these shaders
            handle = GL.CreateProgram();

            GL.AttachShader(handle, vertexShader);
            GL.AttachShader(handle, fragmentShader);

            LinkProgram(handle);

            // Cleanup the individual shaders that are useless now that they have been linked
            GL.DetachShader(handle, vertexShader);
            GL.DetachShader(handle, fragmentShader);
            GL.DeleteShader(fragmentShader);
            GL.DeleteShader(vertexShader);

            // Get the number of active uniforms in the shader.
            GL.GetProgram(handle, GetProgramParameterName.ActiveUniforms, out var numberOfUniforms);

            // Allocate a disctionnary to hold the locations
            uniformLocations = new Dictionary<string, int>();

            // Loop over all the uniforms
            for (int i = 0; i < numberOfUniforms; i++)
            {
                // Get the name of the uniform
                string key = GL.GetActiveUniform(handle, i, out _, out _);

                // Get the location
                int location = GL.GetUniformLocation(handle, key);

                uniformLocations.Add(key, location);
            }
        }

        private static void CompileShader(int shader)
        {
            // Compile the shader
            GL.CompileShader(shader);

            // Check for compilation errors
            GL.GetShader(shader, ShaderParameter.CompileStatus, out var code);

            if(code != (int)All.True)
            {
                string infoLog = GL.GetShaderInfoLog(shader);
                throw new Exception($"Error occurred whilst compiling Shader({shader}).\n\n{infoLog}");
            }
        }

        private static void LinkProgram(int program)
        {
            // Link the program
            GL.LinkProgram(program);

            // Check for linking errors
            GL.GetProgram(program, GetProgramParameterName.LinkStatus, out var code);
            if (code != (int)All.True)
            {
                throw new Exception($"Error occured whilst linking Program({program})");
            }
        }

        private static string ReadShaderSource(string shaderPath)
        {
            using StreamReader reader = new StreamReader(shaderPath, Encoding.UTF8);
            return reader.ReadToEnd();
        }

        public void Use()
        {
            GL.UseProgram(handle);
        }

        public int GetAttribLocation(string attribName)
        {
            return GL.GetAttribLocation(handle, attribName);
        }

        public void SetInt(string name, int value)
        {
            int location = GL.GetUniformLocation(handle, name);

            GL.Uniform1(location, value);
        }

        public void SetMatrix4(string name, Matrix4 data)
        {
            GL.UseProgram(handle);
            GL.UniformMatrix4(uniformLocations[name], true, ref data);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                GL.DeleteProgram(handle);
                disposedValue = true;
            }
        }

        ~Shader()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(false);
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
