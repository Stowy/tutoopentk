﻿using OpenToolkit.Windowing.Desktop;
using OpenToolkit.Mathematics;

namespace TutoOpenTK
{
    class Program
    {
        static void Main()
        {
            GameWindowSettings gameWindowSettings = new GameWindowSettings();

            NativeWindowSettings nativeWindowSettings = new NativeWindowSettings
            {
                Size = new Vector2i(800, 600),
                Title = "Learn OpenTK"
            };

            using Game game = new Game(gameWindowSettings, nativeWindowSettings);
            game.Run();
        }
    }
}
